<?php
 include "header.php";
?>
    <!-- contact section -->
<section id="contact" class="text-center">
 	<div class="container section-contact">
		<div class="row">
			<div class="col-lg-12 text-center">
				<div class="section-title">
					<h2>ENTRE EM CONTATO CONOSCO</h2>
					<h5>"SUA MENSAGEM É TRATADA COM A PRIORIDADE QUE MERECE"</h5>
                    <hr class="bottom-line">
              <h3><i class="fa fa-phone"></i> +55 32 3021-7545<i class="fa fa-whatsapp"></i></h3>
				</div>
				<div class="contact-form wow fadeInUp" data-wow-delay="1.0s">
					<form id="contact-form" method="post" action="#">
                        <div class="col-md-6 col-sm-6">
                          	<input name="name" type="text" class="form-control" placeholder="Seu nome..." required>
                        </div>
                        <div class="col-md-6 col-sm-6">
                          	<input name="email" type="email" class="form-control" placeholder="Seu e-mail..." required>
                        </div>
           			  	<div class="col-md-12 col-sm-12">
				   			<textarea name="message" class="form-control" placeholder="Escreva sua mensagem aqui..." rows="6" required></textarea>
           			  	</div>
						<div class="col-md-offset-3 col-md-6 col-sm-offset-2 col-sm-8">
							<input name="submit" type="submit" class="form-control submit" id="submit" value="ENVIE SUA MENSAGEM">
						</div>
					</form>
				</div>
			</div>
            <div class="section-title">
					<h2>VOCÊ PODE NOS ENCONTRAR NO ENDEREÇO:</h2>
					<h5>RUA GERALDO MAGELA FERNANDES, 210 - UBA | MG</h5>
                    <hr class="bottom-line">
				</div>
		</div>
	</div>
    <di>
    <div class="container">
		<div class="row">
    <div class="col-lg-12 text-center maps">
     <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3722.447978055997!2d-42.95326068549401!3d-21.094698985962708!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xa31bd1f73e8d75%3A0x6974f9f93d8e2689!2sR.+Geraldo+Magela+Fernandes%2C+210+-+Uba%2C+Ub%C3%A1+-+MG%2C+36500-000!5e0!3m2!1spt-BR!2sbr!4v1517359981342" width="100%" height="400" frameborder="0" style="border:01px solid #d2ae6d" allowfullscreen></iframe>
     </div>    
    </div>
    </div>
    </div>
</section>

 <?php
 include "footer.php";
 ?>