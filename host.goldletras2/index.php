<?php
 include "header.php";
?>
	
	<section id="home" class="text-center"><!--/start-slider-home-->
		    <div class="flexslider">
		      <ul class="slides">
		        <li>
		        	<img src="images/home/slider/slider-img13.jpg" alt="Slide 1">
		        	<div class="slider-caption">
					    <div class="templatemo_homewrapper">
					      <h1>5 razões para você contratar nosso serviço de letra caixa:</h1>
                          <h2 class="wow fadeInDown" data-wow-delay="2000">
			<span class="rotate blue">Sem mão de obra mais tempo para outros processos do projeto;, Preço de fábrica. Baixo custo e melhor qualidade para cliente final;, Corte a laser e acabamento perfeito. 100% Personalizados!, Solda fria e sem marcas na parte externa das letras;
, 3 tipos de fixação e maior segurança sem risco de soltar.</span></h2>	
<a href="contato.php" class="btn" title="Solicite agora o seu orçamento!">SOLICITE ORÇAMENTO</a>
					    </div>
				  	</div>
		        </li>
		        <li>
		        	<img src="images/home/slider/slider-img2.jpg" alt="Slide 2">
		        	<div class="slider-caption">
					    <div class="templatemo_homewrapper">
					      <h1 class="wow fadeInDown" data-wow-delay="2000"></h1>
                          <h2 class="wow fadeInDown" data-wow-delay="2000">
			<span class="rotate blue"></span></h2>	
<a href="contato.php" class="btn" title="Solicite agora o seu orçamento!">SOLICITE ORÇAMENTO</a>
					    </div>
				  	</div>
		        </li>
		          <li>
		        	<img src="images/home/slider/slider-img3.jpg" alt="Slide 3">
		        	<div class="slider-caption">
					    <div class="templatemo_homewrapper">
					      <h1 class="wow fadeInDown" data-wow-delay="2000"></h1>
                          <h2 class="wow fadeInDown" data-wow-delay="2000">
			<span class="rotate blue"></span></h2>	
<a href="contato.php" class="btn" title="Solicite agora o seu orçamento!">SOLICITE ORÇAMENTO</a>
					    </div>
				  	</div>
		        </li> 
		        <li>
		        	<img src="images/home/slider/slider-img4.jpg" alt="Slide 4">
		        	<div class="slider-caption">
					    <div class="templatemo_homewrapper">
					      <h1 class="wow fadeInDown" data-wow-delay="2000"></h1>
                          <h2 class="wow fadeInDown" data-wow-delay="2000">
			<span class="rotate blue"></span></h2>	
<a href="contato.php" class="btn" title="Solicite agora o seu orçamento!">SOLICITE ORÇAMENTO</a>
					    </div>
				  	</div>
		        </li> 
		         <li>
		        	<img src="images/home/slider/slider-img5.jpg" alt="Slide 5">
		        	<div class="slider-caption">
					    <div class="templatemo_homewrapper">
					      <h1 class="wow fadeInDown" data-wow-delay="2000"></h1>
                          <h2 class="wow fadeInDown" data-wow-delay="2000">
			<span class="rotate blue"></span></h2>	
<a href="contato.php" class="btn" title="Solicite agora o seu orçamento!">SOLICITE ORÇAMENTO</a>
					    </div>
				  	</div>
		        </li>
              </ul>
		    </div>		
		</section><!--/end-slider-home-->
    
    <!-- start products -->
		<section id="products">
			<div class="container section-products">
				<div class="col-lg-12 text-center">
					<h2>AÇO INOX POLIDO</h2>
                    <h5>"O REQUINTE E A SOFISTICAÇÃO QUE O SEU PROJETO MERECE"</h5>
                    <hr class="bottom-line">
				</div>
				<div class="row">
                <div class="col-sm-6 wow fadeInLeft"  data-wow-delay="2000">
                 <figure><img src="images/home/img_politori.jpg" class="img-responsive" title="Politori&Toledo Advogados"  alt="imagem politori&toledo advogados"></figure>
				</div>
				<div class="col-sm-6  wow fadeInLeft justify" data-wow-delay="2000">
				<p>Letras de aço inox polido são as chamadas letras em alto relevo, letras caixa ou block-letters em aço inox polido (brilhantes). Estas letras e logotipos em aço polido podem ser confeccionadas em vários tamanhos e formatos, nossa empresa possui corte à laser, o que permite que o projeto fique conforme apresentado.
                <p>As letras em aço inox polido possuem acabamento especial podendo ser utilizadas em escritórios de advocacia, arquitetura, empresas, fachadas de lojas, stand de vendas de construtoras, escolas, recepções, hall de elevadores, igrejas, entre outros estabelecimentos comerciais...</p>
                         <div class="row" align="center"><a href="produtos.php" class="btn btn-products">SAIBA MAIS <i class="fa fa-angle-double-right"></i></a></div>
                     </div>
				</div>
			</div>
		</section>
		<!-- end products -->
        
        <!-- start products -->
		<section id="products">
			<div class="container section-products">
				<div class="col-lg-12 text-center">
					<h2>AÇO INOX ESCOVADO</h2>
                    <h5>"O REQUINTE E A SOFISTICAÇÃO QUE O SEU PROJETO MERECE"</h5>
                    <hr class="bottom-line">
				</div>
				<div class="row">
                <div class="col-sm-6  wow fadeInLeft justify" data-wow-delay="2000">
				<p>Letras em aço inox escovado são as chamadas letras em alto relevo, letras caixa ou block-letters em aço escovado. Estas letras e logotipos em aço escovado podem ser confeccionadas em vários tamanhos e formatos, nossa empresa possui corte à laser, o que permite que o projeto fique conforme presentado. 
<p>As letras em aço escovado possuem acabamento especial,
podendo ser utilizadas em escritórios de advocacia, arquitetura, empresas, fachadas de lojas, stand de vendas de construtoras, escolas, recepções, hall de elevadores, igrejas, entre outros estabelecimentos comerciais...</p>
                         <div class="row" align="center"><a href="produtos.php" class="btn btn-products">SAIBA MAIS <i class="fa fa-angle-double-right"></i></a></div>
                       </div>
                <div class="col-sm-6 wow fadeInLeft"  data-wow-delay="2000">
                 <figure><img src="images/home/img_magnaghi.jpg" class="img-responsive" title="Magnaghi Friuli Aerospace"  alt="imagem magnaghi friuli aerospace"></figure>
				</div>
				</div>
			</div>
		</section>
		<!-- end products -->
        
        <!-- start products -->
		<section id="products">
			<div class="container section-products">
				<div class="col-lg-12 text-center">
					<h2>CHAPA GALVANIZADA</h2>
                    <h5>"O REQUINTE E A SOFISTICAÇÃO QUE O SEU PROJETO MERECE"</h5>
                    <hr class="bottom-line">
				</div>
				<div class="row">
                <div class="col-sm-6 wow fadeInLeft"  data-wow-delay="2000">
                 <figure><img src="images/home/img_procriativo.jpg" class="img-responsive" title="Procriativo Comunicação"alt="imagem procriativo comunicação"></figure>
				</div>
				<div class="col-sm-6  wow fadeInLeft justify " data-wow-delay="2000">
				<p>Letras de chapa galvanizada são as chamadas letras em alto relevo, letras caixa ou block-letters em chapa galvanizada. Estas letras e logotipos em chapa galvanizada podem ser confeccionadas em vários tamanhos e formatos, nossa empresa possui corte à laser, o que permite que o projeto fique conforme apresentado.
<p>As letras em chapa galvanizada possuem acabamento especial podendo ser utilizadas em escritórios de advocacia, Arquitetura, empresas, fachadas de lojas, stand de vendas de construtoras, escolas, recepções, hall de elevadores, igrejas, entre outros estabelecimentos comerciais...</p>
                         <div class="row" align="center"><a href="produtos.php" class="btn btn-products">SAIBA MAIS <i class="fa fa-angle-double-right"></i></a></div>
                     </div>
				</div>
			</div>
		</section>
		<!-- end products -->
         <!-- start products -->
		<section id="products">
			<div class="container section-products">
				<div class="col-lg-12 text-center">
					<h2>LATÃO POLIDO</h2>
                    <h5>"O REQUINTE E A SOFISTICAÇÃO QUE O SEU PROJETO MERECE"</h5>
                    <hr class="bottom-line">
				</div>
				<div class="row">
                <div class="col-sm-6  wow fadeInLeft justify " data-wow-delay="2000">
				<p>Letras em latão polido são as chamadas letras em alto relevo, letras caixa ou block-letters em latão polido. Estas letras e logotipos em latão polido podem ser confeccionadas em vários tamanhos e formatos, nossa empresa possui corte à laser, o que permite que o projeto fique conforme apresentado.
<p>As letras em latão polido possuem acabamento especial podendo ser utilizadas em escritórios de advocacia, arquitetura, empresas, fachadas de lojas, condomínios, igrejas, entre outros estabelecimentos comerciais. As letras de latão polido são ideais para utilização em ambientes externos...</p>
                         <div class="row" align="center"><a href="produtos.php" class="btn btn-products">SAIBA MAIS <i class="fa fa-angle-double-right"></i></a></div>
                       </div>
                <div class="col-sm-6 wow fadeInLeft"  data-wow-delay="2000">
                 <figure><img src="images/home/img_intimit.jpg" class="img-responsive" title="Intimit"  alt="imagem intimit"></figure>
				</div>
				</div>
			</div>
		</section>
		<!-- end products -->    
    
    <!-- start depoimentos -->
		<section id="testimonials">
			<div class="container section-testimonials">
				<div class="col-lg-12 text-center">
					<h2>DEPOIMENTOS</h2>
                    <h5>"A SATISFAÇÃO DOS NOSSOS CLIENTES MOVEM NOSSOS SONHOS!"</h5>
                    <hr class="bottom-line">
				</div><div class="row">
					<div class="recommended_items">
				    	<div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
							<div class="carousel-inner">
								<div class="item active">	
									<div class="col-sm-4">
												<div class="testimonials-partners text-center">
												<h2>André L. Oliveira</h2>
													<p>"...terceirizar serviços é algo que requer muito cuidado e afinidade com ideais de gestão e logística, pois estamos confiando a outros a credibilidade da nossa empresa. E a parceria com a Gold Letras..."</p>
													<a href="https://www.facebook.com/pg/goldletras/reviews/?ref=page_internal" target="_blank" title="Confira na Íntegra" class="btn-testimonials">Conferir na íntegra</a>
												</div>
									</div>
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="testimonials-partners text-center">
													<h2>Reuller Caetano</h2>
													<p>"Atendimento de excelente qualidade, muita presteza em esclarecer todas as duvidas e enviar rapidamente o orçamento! Preço justo, material e trabalho com muita qualidade e e as letras feitas com..."</p>
													<a href="https://www.facebook.com/pg/goldletras/reviews/?ref=page_internal" target="_blank" title="Confira na Íntegra" class="btn-testimonials">Conferir na íntegra</a>
												</div>
											</div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="testimonials-partners text-center">
				     								<h2>Ediney Gomes</h2>
													<p>"Atendimento excelente, muito prestativo!!! Preço incomparável, material e trabalho com qualidade e a entrega realizada dentro do prazo... Fiquei muito feliz e satisfeito com o resultado, na realidade..."</p>
													<a href="https://www.facebook.com/pg/goldletras/reviews/?ref=page_internal" target="_blank" title="Confira na Íntegra" class="btn-testimonials">Conferir na íntegra</a>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="item">	
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="testimonials-partners text-center">
													<h2>Italo Carmo Patricio</h2>
													<p>"Entrega rápida, produto de excelência, atendimento impecável,recomendo, só penso que merecia um prêmio ou um brinde quem comprar algo acima de 500,00..."</p>
													<a href="https://www.facebook.com/pg/goldletras/reviews/?ref=page_internal" target="_blank" title="Confira na Íntegra" class="btn-testimonials">Conferir na íntegra</a>
												</div>
											</div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="testimonials-partners text-center">
													<h2>Renata Oliveira</h2>
													<p>"Eu recomendo, empresa atendeu muito bem os produtos são de primeira qualidade, entregaram no prazo. 
Fiquei satisfeito com a compra, empresa responsável e pontual..."</p>
													<a href="https://www.facebook.com/pg/goldletras/reviews/?ref=page_internal" target="_blank" title="Confira na Íntegra" class="btn-testimonials">Conferir na íntegra</a>
												</div>
											</div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="testimonials-partners text-center">
													<h2>Sandro Alcantara</h2>
													<p>"Trabalho, qualidade e atendimento excelentes. Foi muito bom contratar os serviços desta empresa. Chegou no prazo e do jeito que foi solicitado..."</p>
													<a href="https://www.facebook.com/pg/goldletras/reviews/?ref=page_internal" target="_blank" title="Confira na Íntegra" class="btn-testimonials">Conferir na íntegra</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
                            <!--
							  <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
								<i class="fa fa-angle-left"></i>
							  </a>
							  <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
								<i class="fa fa-angle-right"></i>
							  </a>
                             -->
						</div>
					</div><!--/depoimentos-->
					
				   </div>
                </div>
		</section>
 <?php
 include "footer.php";
 ?>