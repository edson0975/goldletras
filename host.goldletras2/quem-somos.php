<?php
 include "header.php";
?>
    <!-- contact section -->
<section id="contact" class="text-center">
 	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<div class="section-title">
					<h2>QUEM SOMOS</h2>
					<h5>"UM NEGÓCIO SÓ É BOM QUANDO AS DUAS PARTES GANHAM"</h5>
                    <hr class="bottom-line">             
				</div>
                <div class="col-sm-6 wow fadeInLeft"  data-wow-delay="2000">
                <figure>
				<img src="images/about/logotipo_gold_letras_quem_somos.png" class="quem_somos img-responsive"/>
				</figure>
                </div>
                <div class="col-sm-6  wow fadeInLeft" data-wow-delay="2000">
						<p><strong>MISSÃO:</strong> Garantir a satisfação do cliente com soluções criativas, ideias inovadoras e serviços de qualidade que superem suas expectativas. Fazer com que cada cliente seja reconhecido como autoridade em seu segmento de atuação. Agregar valor ao negócio, potencializar o crescimento das operações e promover e estreitar o relacionamento do cliente com o seu público alvo, por meio da geração de conteúdo de relevância.</p>
						<p><strong>VISÃO:</strong> Estar entre as 5 maiores empresas do mundo, buscando ainda mais qualidade, inovação, satisfação, rentabilidade e ser uma das melhores empresas para se trabalhar.</p>
						<p><strong>VALORES:</strong> Inovação: a Gold Letras é uma empresa sintonizada com as tendências do mercado, possuindo um portfolio de alta qualidade e avançado design, garantindo a seus clientes e consumidores soluções atuais e personalizadas.</p>
                        <hr class="bottom-line">
                        </div>
                        
                        
                        <div class="col-sm-6  wow fadeInLeft" data-wow-delay="2000">
						<p><strong>Qualidade:</strong> a empresa prioriza os princípios da qualidade total em suas atividades, seguindo a prática de melhorias contínuas, visando alcançar seus objetivos: de tempo, inovação e custos.</p>
						<p><strong>Integridade:</strong> é uma empresa reconhecida pela ética com que conduz os negócios, pela integridade de seus profissionais, pelo relacionamento duradouro com clientes, consumidores e fornecedores e pelo cumprimento de suas obrigações sociais.</p>
						<p><strong>Profissionais:</strong> a Gold Letras, olha e respeita sua equipe, como principal fator de seu sucesso e
						desenvolvimento, desta forma, realiza ações de engajamento, conectando seus profissionais ao jeito "Gold Letras de Ser".</p>
						<p><strong>Lucratividade:</strong> a rentabilidade é a medida do seu desempenho. O lucro garante a remuneração dos acionistas, o desenvolvimento da empresa e o bem estar dos profissionais que nela atuam.</p>
                        <hr class="bottom-line">
                        </div>
                        <div class="col-sm-6 wow fadeInLeft"  data-wow-delay="2000">
                <figure>
				<img src="images/about/img_quem_somos.jpg" class="quem_somos img-responsive" " />
				</figure>
                </div>                        
                    </div>
            <div class=" col-sm-12 section-title">
					<h2>SERÁ UMA SATISFAÇÃO ATENDÊ-LOS!</h2>
					<h5>"CONTE SEMPRE CONOSCO"</h5>
                    <hr class="bottom-line">
			</div>
		</div>
	</div>
    <di>
    
    </div>
</section>

 <?php
 include "footer.php";
 ?>