<?php
 include "header.php";
?>
    <!-- contact section -->
<section id="contact" class="text-center">
 	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<div class="section-title">
					<h2>PORTFÓLIO</h2>
					<h5>"CLIENTES GOLD LETRAS"</h5>
                    <hr class="bottom-line">             
				</div>
             <!-- Start WOWSlider.com BODY section -->
<div id="wowslider-container1">
	<div class="ws_images">
		<ul>
			<li><img src="data1/images/galvanizada_01.jpg" alt="galvanizada_01" title="galvanizada_01" id="wows1_0"/></li>
			<li><img src="data1/images/galvanizada_02.jpg" alt="galvanizada_02" title="galvanizada_02" id="wows1_1"/></li>
			<li><img src="data1/images/galvanizada_03.jpg" alt="galvanizada_03" title="galvanizada_03" id="wows1_2"/></li>
			<li><img src="data1/images/galvanizada_04.jpg" alt="galvanizada_04" title="galvanizada_04" id="wows1_3"/></li>
			<li><img src="data1/images/galvanizada_05.jpg" alt="galvanizada_05" title="galvanizada_05" id="wows1_4"/></li>
			<li><img src="data1/images/inox_01.jpg" alt="inox_01" title="inox_01" id="wows1_5"/></li>
			<li><img src="data1/images/inox_02.jpg" alt="inox_02" title="inox_02" id="wows1_6"/></li>
			<li><img src="data1/images/inox_03.jpg" alt="inox_03" title="inox_03" id="wows1_7"/></li>
			<li><img src="data1/images/inox_04.jpg" alt="inox_04" title="inox_04" id="wows1_8"/></li>
			<li><img src="data1/images/inox_05.jpg" alt="inox_05" title="inox_05" id="wows1_9"/></li>
			<li><img src="data1/images/inox_06.jpg" alt="inox_06" title="inox_06" id="wows1_10"/></li>
			<li><img src="data1/images/inox_07.jpg" alt="inox_07" title="inox_07" id="wows1_11"/></li>
			<li><img src="data1/images/inox_08.jpg" alt="inox_08" title="inox_08" id="wows1_12"/></li>
			<li><img src="data1/images/inox_09.jpg" alt="inox_09" title="inox_09" id="wows1_13"/></li>
			<li><img src="data1/images/inox_10.jpg" alt="inox_10" title="inox_10" id="wows1_14"/></li>
			<li><img src="data1/images/inox_11.jpg" alt="inox_11" title="inox_11" id="wows1_15"/></li>
			<li><img src="data1/images/inox_12.jpg" alt="inox_12" title="inox_12" id="wows1_16"/></li>
			<li><img src="data1/images/inox_13.jpg" alt="inox_13" title="inox_13" id="wows1_17"/></li>
			<li><img src="data1/images/inox_14.jpg" alt="inox_14" title="inox_14" id="wows1_18"/></li>
			<li><img src="data1/images/inox_15.jpg" alt="inox_15" title="inox_15" id="wows1_19"/></li>
			<li><img src="data1/images/inox_16.jpg" alt="inox_16" title="inox_16" id="wows1_20"/></li>
			<li><img src="data1/images/inox_17.jpg" alt="inox_17" title="inox_17" id="wows1_21"/></li>
			<li><img src="data1/images/inox_18.jpg" alt="inox_18" title="inox_18" id="wows1_22"/></li>
			<li><img src="data1/images/polido_01.jpg" alt="polido_01" title="polido_01" id="wows1_23"/></li>
			<li><img src="data1/images/polido_02.jpg" alt="polido_02" title="polido_02" id="wows1_24"/></li>
			<li><img src="data1/images/producao_01.jpg" alt="producao_01" title="producao_01" id="wows1_25"/></li>
			<li><img src="data1/images/producao_02.jpg" alt="producao_02" title="producao_02" id="wows1_26"/></li>
			<li><img src="data1/images/producao_03.jpg" alt="producao_03" title="producao_03" id="wows1_27"/></li>
			<li><img src="data1/images/producao_04.jpg" alt="producao_04" title="producao_04" id="wows1_28"/></li>
			<li><img src="data1/images/producao_05.jpg" alt="producao_05" title="producao_05" id="wows1_29"/></li>
			<li><img src="data1/images/producao_06.jpg" alt="producao_06" title="producao_06" id="wows1_30"/></li>
			<li><img src="data1/images/producao_07.jpg" alt="producao_07" title="producao_07" id="wows1_31"/></li>
			<li><img src="data1/images/producao_08.jpg" alt="producao_08" title="producao_08" id="wows1_32"/></li>
			<li><a href="http://wowslider.net"><img src="data1/images/producao_09.jpg" alt="javascript photo gallery" title="producao_09" id="wows1_33"/></a></li>
			<li><img src="data1/images/producao_10.jpg" alt="producao_10" title="producao_10" id="wows1_34"/></li>
	</ul>
</div>
	<div class="ws_thumbs">
<div>
		<a href="#" title="galvanizada_01"><img src="data1/tooltips/galvanizada_01.jpg" alt="" /></a>
		<a href="#" title="galvanizada_02"><img src="data1/tooltips/galvanizada_02.jpg" alt="" /></a>
		<a href="#" title="galvanizada_03"><img src="data1/tooltips/galvanizada_03.jpg" alt="" /></a>
		<a href="#" title="galvanizada_04"><img src="data1/tooltips/galvanizada_04.jpg" alt="" /></a>
		<a href="#" title="galvanizada_05"><img src="data1/tooltips/galvanizada_05.jpg" alt="" /></a>
		<a href="#" title="inox_01"><img src="data1/tooltips/inox_01.jpg" alt="" /></a>
		<a href="#" title="inox_02"><img src="data1/tooltips/inox_02.jpg" alt="" /></a>
		<a href="#" title="inox_03"><img src="data1/tooltips/inox_03.jpg" alt="" /></a>
		<a href="#" title="inox_04"><img src="data1/tooltips/inox_04.jpg" alt="" /></a>
		<a href="#" title="inox_05"><img src="data1/tooltips/inox_05.jpg" alt="" /></a>
		<a href="#" title="inox_06"><img src="data1/tooltips/inox_06.jpg" alt="" /></a>
		<a href="#" title="inox_07"><img src="data1/tooltips/inox_07.jpg" alt="" /></a>
		<a href="#" title="inox_08"><img src="data1/tooltips/inox_08.jpg" alt="" /></a>
		<a href="#" title="inox_09"><img src="data1/tooltips/inox_09.jpg" alt="" /></a>
		<a href="#" title="inox_10"><img src="data1/tooltips/inox_10.jpg" alt="" /></a>
		<a href="#" title="inox_11"><img src="data1/tooltips/inox_11.jpg" alt="" /></a>
		<a href="#" title="inox_12"><img src="data1/tooltips/inox_12.jpg" alt="" /></a>
		<a href="#" title="inox_13"><img src="data1/tooltips/inox_13.jpg" alt="" /></a>
		<a href="#" title="inox_14"><img src="data1/tooltips/inox_14.jpg" alt="" /></a>
		<a href="#" title="inox_15"><img src="data1/tooltips/inox_15.jpg" alt="" /></a>
		<a href="#" title="inox_16"><img src="data1/tooltips/inox_16.jpg" alt="" /></a>
		<a href="#" title="inox_17"><img src="data1/tooltips/inox_17.jpg" alt="" /></a>
		<a href="#" title="inox_18"><img src="data1/tooltips/inox_18.jpg" alt="" /></a>
		<a href="#" title="polido_01"><img src="data1/tooltips/polido_01.jpg" alt="" /></a>
		<a href="#" title="polido_02"><img src="data1/tooltips/polido_02.jpg" alt="" /></a>
		<a href="#" title="producao_01"><img src="data1/tooltips/producao_01.jpg" alt="" /></a>
		<a href="#" title="producao_02"><img src="data1/tooltips/producao_02.jpg" alt="" /></a>
		<a href="#" title="producao_03"><img src="data1/tooltips/producao_03.jpg" alt="" /></a>
		<a href="#" title="producao_04"><img src="data1/tooltips/producao_04.jpg" alt="" /></a>
		<a href="#" title="producao_05"><img src="data1/tooltips/producao_05.jpg" alt="" /></a>
		<a href="#" title="producao_06"><img src="data1/tooltips/producao_06.jpg" alt="" /></a>
		<a href="#" title="producao_07"><img src="data1/tooltips/producao_07.jpg" alt="" /></a>
		<a href="#" title="producao_08"><img src="data1/tooltips/producao_08.jpg" alt="" /></a>
		<a href="#" title="producao_09"><img src="data1/tooltips/producao_09.jpg" alt="" /></a>
		<a href="#" title="producao_10"><img src="data1/tooltips/producao_10.jpg" alt="" /></a>
	</div>
  </div>
</div>	
<script type="text/javascript" src="engine1/wowslider.js"></script>
<script type="text/javascript" src="engine1/script.js"></script>
                      </div>
                <!--<div class="section-title">
					<h2>A MELHOR MANEIRA DE SATISFAZER O SEU CLIENTE É FAZENDO O QUE SE AMA!</h2>
					<h5>"CONTE SEMPRE CONOSCO"</h5>
                    <hr class="bottom-line">
				</div>-->
		</div>
	</div>
    <di>
    
    </div>
</section>
 <?php
 include "footer.php";
 ?>