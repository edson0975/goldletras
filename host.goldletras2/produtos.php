<?php
 include "header.php";
?>
    
<!-- start products -->
		<section id="products">
			<div class="container section-products">
				<div class="col-lg-12 text-center">
					<h2>PRODUTO</h2>
                    <h5>"SATISFAÇÃO É QUANDO ENTREGAMOS MAIS DO QUE PROMETEMOS"</h5>
                    <hr class="bottom-line">
				</div>
				<div class="row">
                <div class="col-sm-6 wow fadeInLeft"  data-wow-delay="2000">
                 <figure><img src="images/home/img_politori.jpg" class="img-responsive" title="Politori&Toledo Advogados"  alt="imagem politori&toledo advogados"></figure>
				</div>
				<div class="col-sm-6  wow fadeInLeft justify" data-wow-delay="2000">
				<p>Letras de aço inox polido são as chamadas letras em alto relevo, letras caixa ou block-letters em aço inox polido (brilhantes). Estas letras e logotipos em aço polido podem ser confeccionadas em vários tamanhos e formatos, nossa empresa possui corte à laser, o que permite que o projeto fique conforme apresentado.
                <p>As letras em aço inox polido possuem acabamento especial podendo ser utilizadas em escritórios de advocacia, arquitetura, empresas, fachadas de lojas, stand de vendas de construtoras, escolas, recepções, hall de elevadores, igrejas, entre outros estabelecimentos comerciais.
                <p>As letras de aço inox polido são ideais para utilização em ambientes externos, principalmente em beira de praias, mas também podem ser utilizadas em
ambientes internos sem perder o seu requinte e sofisticação.
                <p>As letras de aço inox polido possuem um processo de instalação bem simples, nós mandamos todo o material necessário para você mesmo instalar, molde para instalação das letras conforme o projeto, utilizando para sua fixação pinos de encaixe 5/32 ou com uma base para colar (letras em aço polido coladas em vidro ou
paredes lisas).
                <p>Temos três tipos de fixação, o cliente escolhe no momento do pedido, qual se adapta melhor na superfície que serão instaladas as letras, nossa empresa dá todo o suporte necessário para que o projeto saia perfeito.</p>
                     </div>
				</div>
			</div>
		</section>
		<!-- end products -->
        
        <!-- start products -->
		<section id="products">
			<div class="container section-products">
				<div class="col-lg-12 text-center">
					<h2>AÇO INOX ESCOVADO</h2>
                    <h5>"O REQUINTE E A SOFISTICAÇÃO QUE O SEU PROJETO MERECE"</h5>
                    <hr class="bottom-line">
				</div>
				<div class="row">
                <div class="col-sm-6  wow fadeInLeft justify" data-wow-delay="2000">
				<p>Letras em aço inox escovado são as chamadas letras em alto relevo, letras caixa ou block-letters em aço escovado. Estas letras e logotipos em aço escovado podem ser confeccionadas em vários tamanhos e formatos, nossa empresa possui corte à laser, o que permite que o projeto fique conforme presentado. 
<p>As letras em aço escovado possuem acabamento especial,
podendo ser utilizadas em escritórios de advocacia, arquitetura, empresas, fachadas de lojas, stand de vendas de construtoras, escolas, recepções, hall de elevadores, igrejas, entre outros estabelecimentos comerciais...
<p>As letras em aço escovado são ideais para utilização em ambientes externos, principalmente em beira de praias, mas também podem ser utilizadas em ambientes internos sem perder o seu requinte e sofisticação.
<p>As letras em aço escovado possuem um processo de instalação bem simples, nós mandamos todo o material necessário para você mesmo instalar, molde para instalação das letras conforme o projeto, utilizando para sua fixação pinos de encaixe 5/32 ou com uma base para colar (letras em aço polido coladas em vidro ou
paredes lisas).
<p>Temos três tipos de fixação, o cliente escolhe no momento do pedido, qual se adapta melhor na superfície que serão instaladas as letras, nossa empresa dá todo o suporte necessário para que o projeto saia perfeito.</p>
                       </div>
                <div class="col-sm-6 wow fadeInLeft"  data-wow-delay="2000">
                 <figure><img src="images/home/img_magnaghi.jpg" class="img-responsive" title="Magnaghi Friuli Aerospace"  alt="imagem magnaghi friuli aerospace"></figure>
				</div>
				</div>
			</div>
		</section>
		<!-- end products -->
        
        <!-- start products -->
		<section id="products">
			<div class="container section-products">
				<div class="col-lg-12 text-center">
					<h2>CHAPA GALVANIZADA</h2>
                    <h5>"O REQUINTE E A SOFISTICAÇÃO QUE O SEU PROJETO MERECE"</h5>
                    <hr class="bottom-line">
				</div>
				<div class="row">
                <div class="col-sm-6 wow fadeInLeft"  data-wow-delay="2000">
                 <figure><img src="images/home/img_procriativo.jpg" class="img-responsive" title="Procriativo Comunicação"alt="imagem procriativo comunicação"></figure>
				</div>
				<div class="col-sm-6  wow fadeInLeft justify " data-wow-delay="2000">
				<p>Letras de chapa galvanizada são as chamadas letras em alto relevo, letras caixa ou block-letters em chapa galvanizada. Estas letras e logotipos em chapa galvanizada podem ser confeccionadas em vários tamanhos e formatos, nossa empresa possui corte à laser, o que permite que o projeto fique conforme apresentado.
<p>As letras em chapa galvanizada possuem acabamento especial podendo ser utilizadas em escritórios de advocacia, Arquitetura, empresas, fachadas de lojas, stand de vendas de construtoras, escolas, recepções, hall de elevadores, igrejas, entre outros estabelecimentos comerciais.
<p>As letras de chapa galvanizada possuem uma vantagem especial, pois além de ter um custo menor, as mesmas sofrem o processo de pintura automotiva, com 1 camada de base tipo wash primer e 2 camadas de tinta automotiva na cor de sua preferência.
<p>As letras e logomarcas em chapa galvanizada são muito utilizadas para reproduzirem logomarcas com cores especiais e degrade e que precisam realmente ser pintadas com tons especiais.
<p>As letras e logomarcas de chapa galvanizada são muito recomendadas para utilização em ambientes internos ou locais onde não sofram com o processo de corrosão (sol – chuva – maresia).
<p>As letras de chapa galvanizada possuem um processo de instalação bem simples, nós mandamos todo o material necessário para você mesmo instalar, molde para instalação das letras conforme o projeto, utilizando para sua fixação pinos de encaixe 5/32 ou com uma base para colar (letras em aço polido coladas em vidro ou paredes lisas).
<p>Temos três tipos de fixação, o cliente escolhe no momento do pedido, qual se adapta melhor na superfície que serão instaladas as letras, nossa empresa dá
todo o suporte necessário para que o projeto saia perfeito.</p>
                     </div>
				</div>
			</div>
		</section>
		<!-- end products -->
         <!-- start products -->
		<section id="products">
			<div class="container section-products">
				<div class="col-lg-12 text-center">
					<h2>LATÃO POLIDO</h2>
                    <h5>"O REQUINTE E A SOFISTICAÇÃO QUE O SEU PROJETO MERECE"</h5>
                    <hr class="bottom-line">
				</div>
				<div class="row">
                <div class="col-sm-6  wow fadeInLeft justify " data-wow-delay="2000">
				<p>Letras em latão polido são as chamadas letras em alto relevo, letras caixa ou block-letters em latão polido. Estas letras e logotipos em latão polido podem ser confeccionadas em vários tamanhos e formatos, nossa empresa possui corte à laser, o que permite que o projeto fique conforme apresentado.
<p>As letras em latão polido possuem acabamento especial podendo ser utilizadas em escritórios de advocacia, arquitetura, empresas, fachadas de lojas, condomínios, igrejas, entre outros estabelecimentos comerciais. As letras de latão polido são ideais para utilização em ambientes externos.
<p>As letras de latão polido são ideais para utilização em ambientes externos desde que se faça uma aplicação de verniz automotivo de alta resistência, principalmente em beira de praias, mas também podem ser utilizadas em ambientes internos sem perder o seu requinte e sofisticação.
<p>As letras de latão polido possuem um processo de instalação bem simples, nós mandamos todo o material necessário para você mesmo instalar, molde para instalação das letras conforme o projeto, utilizando para sua fixação pinos de encaixe 5/32 ou com uma base para colar (letras em aço polido coladas em vidro ou paredes lisas). 
<p>Temos três tipos de fixação, o cliente escolhe no momento do pedido,qual se adapta melhor na superfície que serão instaladas as letras, nossa
empresa dá todo o suporte necessário para que o projeto saia perfeito.</p>
                       </div>
                <div class="col-sm-6 wow fadeInLeft"  data-wow-delay="2000">
                 <figure><img src="images/home/img_intimit.jpg" class="img-responsive" title="Intimit"  alt="imagem intimit"></figure>
				</div>
				</div>
			</div>
		</section>
		<!-- end products -->
 <?php
 include "footer.php";
 ?>