<footer id="footer"><!--Footer-->		
		<div class="footer-widget">
			<div class="container">
				<div class="row">
					<div class="col-sm-3 col-xs-12">
						<div class="single-widget">
							<h2 align="center">Letras Caixas</h2>
                            <hr class="bottom-line">
							<ul class="nav nav-pills nav-stacked">
								<li><a href="produtos.php">aço inox polido</a></li>
								<li><a href="produtos.php">aço inox escovado</a></li>
								<li><a href="produtos.php">chapa galvanizada</a></li>
								<li><a href="produtos.php">latão polido</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-3 col-xs-12">
						<div class="single-widget">
							<h2 align="center">Suporte</h2>
                            <hr class="bottom-line">
							<ul class="nav nav-pills nav-stacked">
								<li><a href="contato.php" title="sac@goldletras.com.br">sac@goldletras.com.br</a></li>
                                <li><a href="contato.php" title="vendas@goldletras.com.br">vendas@goldletras.com.br</a></li>
                                <li><a href="contato.php" title="financeiro@goldletras.com.br">financeiro@goldletras.com.br</a></li>
								<li><a href="https://api.whatsapp.com/send?l=pt_BR&phone=553230217545" target="_blank" title="Cique aqui para direcionar ao Whatsapp">32 3021-7545 <img src="images/home/icon_whatsapp_gold.png" class="whatsapp-business" /></a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-3 col-xs-12">
						<div class="single-widget">
							<h2 align="center">Redes Sociais</h2>
                            <hr class="bottom-line">
							<ul class="nav nav-pills nav-stacked">
								<li><a href="https://plus.google.com/u/1/107474115051831629195" target="_blank" title="Visite nossa página no Google Plus!"><i class="fa fa-google-plus"></i> Google+</a></li>
                                <li><a href="https://www.facebook.com/goldletras/" target="_blank" title="Curta e compartilhe nossa página!"><i class="fa fa-facebook"></i> Facebook</a></li>
                                <li><a href="https://instagram.com/goldletras" target="_blank" title="Veja nossos produtos no Instagram!"><i class="fa fa-instagram"></i> Instagram</a></li>
								<li><a href="https://www.youtube.com/channel/UCbuO54lBukfIi_8LQYfhi4g?view_as=subscriber" target="_blank" title="Inscreva-se em nosso canal!"><i class="fa fa-youtube-play"></i> Youtube</a></li>
								<li><a href="https://www.linkedin.com/company/11297868/" target="_blank" title="Confira nossa página no Linkedin!"><i class="fa fa-linkedin"></i> Linkedin</a></li>
                                </ul>
						</div>
					</div>
					<div class="col-sm-3 col-xs-12">
						<div class="single-widget">
							<h2 align="center">News Letters</h2>
                            <hr class="bottom-line">
							<form action="#" class="searchform">
								<input type="text" placeholder="Endereço de e-mail..." />
								<button type="submit" class="btn btn-default"><i class="fa fa-angle-double-right"></i></button>
								<p>Cadastre seu endereço de email e receba nossos conteúdos gratuitamente!</p>
							</form>
						</div>
					</div>
					
				</div>
			</div>
		</div>
		
		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<p class="text-center">Gold Letras © 2017 - Todos os direitos reservados | Criado por: <span><a target="_blank" href="http://www.criariv.com">Criar Identidade Visual</a></span></p>
				</div>
			</div>
		</div>
		
	</footer><!--/Footer-->
	  
    <script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/price-range.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.flexslider.js"></script>
    <script src="js/main.js"></script>
    
        <!-- start javascript slider -->
		<script src="js/jquery02.js"></script>		
		<script src="js/jquery.simple-text-rotator.js"></script>		
		<script src="js/jquery.flexslider.js"></script>
		<script src="js/templatemo-script.js"></script>
		<!-- end javascript slider -->
</body>
</html>