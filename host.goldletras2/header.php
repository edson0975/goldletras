<!DOCTYPE html>
<html xmlns:s="http://www.w3.org/1999/xhtml" lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Gold Letras | Letras Caixas</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="css/animate.min.css">
    <link rel="stylesheet" href="css/templatemo-style.css">
    <link rel="shortcut icon" type="image/x-icon" href="images/home/4457logotipo-goldletras.ico">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">

		<link rel="stylesheet" type="text/css" href="engine1/style.css" />
		<script type="text/javascript" src="engine1/jquery.js"></script>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script type="text/javascript">
      $(document).ready(function(){
      $("li a[href='"+location.href.substring(location.href.lastIndexOf("/")+1,255)+"']").css("color","#FFF");
      });

  </script>
    
</head><!--/head-->

<body>
		<header id="header"><!--start header-->
		<div class="header_top navbar-fixed-top"><!--start header top-->
			<div class="container"><!--start container-->
				<div class="row"><!--start row-->
					<div class="col-sm-5 col-xs-12">
					 <div class="contactinfo text-center">
					  <ul class="nav nav-pills">
					   <li><a class="phone-contact" href="https://api.whatsapp.com/send?l=pt_BR&phone=553230217545" target="_blank" title="Cique aqui para direcionar ao Whatsapp"><i class="fa fa-phone"></i> 32 3021-7545 <img src="images/home/icon_whatsapp_gold.png" class="whatsapp-business" /></a></li>
					   <li><a class="mail" href="contato.php" title="Clique aqui para enviar-nos um e-mail!"><i class="fa fa-envelope"></i> vendas@goldletras.com.br</a></li>
					  </ul>
                     </div>
                    </div>
                    <div class="col-sm-1 col-xs-12 text-center">
					 <div class="logo text-center">
					 <a href="index.php"><img src="images/home/logotipo_gold_letras.png" alt="Logotipo Gold Letras" title="Logotipo Gold Letras" /></a>
					 </div>	     
					</div>
                    <div class="col-sm-6 pull-right col-xs-12">
                     <div class="mainmenu pull-right">
                      <div class="navbar-header">
					  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					  <span class="sr-only">Toggle navigation</span>
					  <span class="icon-bar"></span>
					  <span class="icon-bar"></span>
					  <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
					  </button>
					  </div>
					  <ul class="nav navbar-nav collapse navbar-collapse">
					   <li><a href="index.php" title="Home">Home</a></li>
                       <li><a href="produtos.php" title="Produto">Produto</a></li>							 
					   <li><a href="quem-somos.php" title="Quem somos">Quem somos</a></li>
					   <li><a href="portfolio.php" title="Portfólio">Portfólio</a></li>
                       <li><a href="contato.php" title="Contato">Contato</a></li>
					  </ul>
					 </div>
                    </div>                        
				</div><!--end row-->
            </div><!--end container-->
		</div><!--end header top-->
		<div class="header-bottom"><!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						

					</div>
				</div>
			</div>
		</div><!--/header-bottom-->
	</header><!-- end header-->